---
title: "Features"
date: 2019-09-04T15:28:16+02:00
draft: false
class: features
intro: "Wax is a word processor for the web. Modern, extensible, responsive, community driven, and 100% open source."
---
