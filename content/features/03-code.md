---
title: "Code block"
draft: false
icon: "images/icons/code.svg"
video: ""
image: "images/Inlinecode.png"
weight: 3
part : 3
---

You can type or paste code inline in any of the languages supported by [Highlight.js](https://highlightjs.org/). 
