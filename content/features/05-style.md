---
title: "Highlighter"
draft: false
icon: "images/icons/styles.svg"
video: ""
image: "images/Textstyles_highlighter.png"
weight: 5
part : 5
---
Highlight text using a variety of colours or choose your own.
