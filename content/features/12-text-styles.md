---
title: "Text Styles"
draft: false
icon: "images/icons/video.svg"
video: "images/Styling.mp4"
image: ""
weight: 10
part: 10
---

Choose from Strong (Bold), Italic, inline Code, Hyperlink, Strikethrough, Underline, Subscript, Superscript, Upper and Lower case.
