---
title: "Spell check"
draft: false
icon: "images/icons/spellCheck.svg"
video: "images/Spellcheck_new.mp4"
image: ""
weight: 9
part : 9
---

Correct misspellings in text. Integration with Grammarly.com browser extensions is also supported. 
