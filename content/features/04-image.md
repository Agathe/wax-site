---
title: "Media"
draft: false
icon: "images/icons/image.svg"
video: "images/Filemanager.mp4"
image: ""
weight: 4
part : 4
---

Use an integrated asset manager to upload images, with the option to include captions.  
