---
title: "Track changes"
draft: false
icon: "images/icons/trackChange.svg"
video: "images/Trackchanges_updated.mp4"
image: ""
weight: 1
part : 1

---

Show, Hide, Accept or Reject suggested changes.
