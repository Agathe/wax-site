---
title: "Tables"
draft: false
icon: "images/icons/tables.svg"
video: "images/Tables_new.mp4"
image: ""
weight: 6
part : 7
---
Easily select and apply the number of columns and rows you need.
