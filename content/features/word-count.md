---
title: "Word count"
draft: false
icon: "images/icons/wordCount.svg"
video: "images/Wordcount_new.mp4"
image: ""
weight: 12
part : 12

---

Values captured for words, characters, characters without spaces, paragraphs, images, tables, footnotes and block-level node counts.
