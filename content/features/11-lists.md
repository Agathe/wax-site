---
title: "Lists"
draft: false
icon: "images/icons/tables.svg"
video: ""
image: "images/Lists.png"
weight: 10
part: 10
---

Create ordered, indented, bulleted and/or numbered lists.
