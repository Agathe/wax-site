---
title: "Math"
draft: false
icon: "images/icons/math.svg"
video: "images/Maths_new.mp4"
image: ""
weight: 2
part : 2
---

Use the LaTex/KaTex library for high-quality technical typesetting.
