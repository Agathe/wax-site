---
title: "Find & replace"
draft: false
icon: "images/icons/findReplace.svg"
video: "images/Findandreplace.mp4"
image: ""
weight: 11
part : 11

---

Easily locate, edit and update text. 
