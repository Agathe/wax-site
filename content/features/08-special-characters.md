---
title: "Special characters"
draft: false
icon: "images/icons/specialChar.svg"
video: "images/Characters.mp4"
image: ""
weight: 8
part : 8
---
Configure, browse or search from a list of characters.
