---
title: "Wax"
draft: false
weight: 1
Intro : "This text will appear in the search result"
class: documentation
---

## The Word Processor
Wax Editor is built against Prosemirror libraries. Check Prosemirror [website](https://prosemirror.net/) and [GitHub repo](https://github.com/ProseMirror) for more information.

<h3> <a href="https://demo.waxjs.net/" target="_blank"> Demo </a> </h3>

### Before Getting Started
  
Wax depends on the following libraries. 

- [React](https://reactjs.org/docs/getting-started.html) for the view(UI)
- [Styled-components](https://styled-components.com/docs) for theming and styling.
- [Inversify.io](http://inversify.io/) as service containers
   
Notice:  Make sure to check them out before starting developing Services, Components, and Theming for Wax. Detailed examples will also follow using the above libraries, but some knowledge is required.

Setting up a full Prosemirror editor  ‘from scratch’, using only the core libraries, requires quite a lot of code.
Wax is broken down into 6 individual packages, which when combined create a full editor, requiring minimum code. On the following overview, we will go through:

- Using the core libraries to create your own editor.
- Using all/some of the already build Services (packages) to customize your editor
- Creating a custom Layout for your needs
- Configuring the already build packages
- Extending the functionality by writing your own Service
- Overview and how you can use certain functionalities provided by Wax, like Overlays, Toolbars, Layouts, and areas, etc.

Latest published versions: 

- wax-prosemirror-core = 0.7.17
- wax-prosemirror-services = 0.7.17

## Quick Overview of  Packages

### Wax Prosemirror Core

Core’s role is

- Mount a prosemirror instance
- Initiate default services
    1. LayoutService
    2. SchemaService
    3. MenuService
    4. PortalService
    5. RulesService
    6. ShortCutsService
- Merge User's config with default config

A big part of wax-core is the application layer, which is responsible for the application’s lifecycle by registering and booting services, merging configs, using the schema, and gathering all prosemirror plugins.

Also holds some default prosemirror plugins that are necessary like the dropCursor, gapCursor, history, and some optional as the placeholder.


```javascript
import { Wax } from "wax-prosemirror-core";
```

`<Wax />` will mount a Prosemirror instance provided that you would pass the configuration that we will see in a later state. 
Once the instance is mounted core’s task is to also communicate through `WaxContext` every change that is happening onto Prosemirror’s state to the Components.

We will go through all of those when we’ll create a React Component for our Editor. 


### Wax Prosemirror Services

Service providers are the central place of Wax bootstrapping. Your own services, as well as all of Wax's core services, are bootstrapped via application provider and are initiated before everything else. But, what do we mean by "bootstrapped"? In general, we mean registering things, including registering service container bindings and event listeners. Service providers are the central place to configure your application. We will dive into detail on how you can create your own Service, what are the Boot and Register methods that you find inside a Service and, extra functionalities already build in which you can use.

## Available Editor Properties , toolgroups and Services

### All available editor properties


```javascript
<Wax
// Sets the focus at the beginning of the document
autoFocus
// When doc changes grab the output
onChange={source=>console.log(source)} 
// output in prose mirror json. (default is HTML)
targetFormat="JSON"
// content
value=""
// editor's config file
config={config}
// readonly mode
readonly
// Editor's layout
layout={myLayout}
//basic image uplading
fileUpload={file=>renderImage(image)}
/>
```

### All currently availabe Services

```javascript
import {
  InlineAnnotationsService,
  AnnotationToolGroupService,
  ImageService,
  ImageToolGroupService,
  LinkService,
  ListsService,
  ListToolGroupService,
  TablesService,
  TableToolGroupService,
  BaseService,
  BaseToolGroupService,
  DisplayBlockLevelService,
  DisplayToolGroupService,
  TextBlockLevelService,
  TextToolGroupService,
  NoteService,
  NoteToolGroupService,
  TrackChangeService,
  CommentsService,
  CodeBlockService,
  CodeBlockToolGroupService,
  DisplayTextToolGroupService,
  MathService,
  FindAndReplaceService,
  EditingSuggestingService,
  TrackingAndEditingToolGroupService,
  FullScreenService,
  FullScreenToolGroupService,
  SpecialCharactersService,
  SpecialCharactersToolGroupService,
  HighlightService,
  TextHighlightToolGroupServices,
  EditorInfoToolGroupServices,
  BottomInfoService,
  TransformService,
  TransformToolGroupService,
  TrackOptionsToolGroupService,
  TrackCommentOptionsToolGroupService,
  CustomTagInlineToolGroupService,
  CustomTagBlockToolGroupService,
  CustomTagService,
} from 'wax-prosemirror-services';
```

### Current Tools and Toolgroups:

- Base Tool group: `undo`, `redo`, `save`

- Inline Annotations Tool group: `strong`, `italic`, `link`, `strikethrough`, `underline`, `subscript`, `superscript`, `small caps`

- Lists Tool group: `numbered list`, `bullet list`

- Image Tool group: `image`

- Table Tool group: `create table`, edit table dropdown that includes: `insert/delete row`, `insert/delete column` `,merge cells`, `split cell`, `delete table`, `Toggle header column`, `Toggle header row`

- Display Tool group: `Title`, `Author`, `Subtitle`,` Epigraph Prose`, `Epigraph Poetry`, `Heading 1`, `Heading 2`,` Heading 3`

- Text Tool group: `Paragraph`, `Paragraph Continued`, `Extract Prose`, `Extract Poetry`, `Source Note`, `Block Quote`

- Notes Tool group: `notes`

- Comments Tool group: `comments` 

- Special Characters group: `special characters`

- Code Block: `code block`

- Tracking and Editing: ``

- Full Screen group: `full screen`
