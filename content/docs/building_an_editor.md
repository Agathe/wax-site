---
title: "Building your first editor"
draft: false
weight: 2
class: documentation
---

## Providing a Layout 

Wax comes without any layout/styles. How the editor will look is controlled by providing a Layout. 
Layout is a react component, which accepts as a prop the mounted prosemirror-surface , which then you can place it, accordingly in your layout.
Check the following example of an editor with a toolbar on the top and a scrollable prosemirror surface.


 **ComponentPlugin is used to  "register" an area within the layout, so you can use it later on in your Service to render components to that
area**

We will see an example of it when we will write our first Service.
 

```javascript
import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { grid, th } from '@pubsweet/ui-toolkit';
import { ComponentPlugin } from 'wax-prosemirror-core';
import { cokoTheme } from '../theme';


const Wrapper = styled.div`
  background: ${th('colorBackground')};
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: 100%;
  line-height: ${grid(4)};
  overflow: hidden;
  width: 100%;

  * {
    box-sizing: border-box;
  }
`;

const Main = styled.div`
  display: flex;
  flex-grow: 1;
  height: calc(100% - 40px);
`;

const TopMenu = styled.div`
  background: ${th('colorBackgroundToolBar')};
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  display: flex;
  height: 40px;
  user-select: none;

  > div:not(:last-child) {
    border-right: ${th('borderWidth')} ${th('borderStyle')}
      ${th('colorFurniture')};
  }
`;

const EditorArea = styled.div`
  flex-grow: 1;
`;

const WaxSurfaceScroll = styled.div`
  box-sizing: border-box;
  display: flex;
  height: 100%;
  overflow-y: auto;
  position: fixed;
  top: 95px;
  width: 100%;
  /* PM styles  for main content*/
  ${EditorElements};
`;

const EditorContainer = styled.div`
  height: 100%;
  width: 100%;

  .ProseMirror {
    height: 100%;
    padding: ${grid(10)};
  }
`;

const MainMenuToolBar = ComponentPlugin('mainMenuToolBar');

const MyLayout = ({ editor }) => {

  return (
    <ThemeProvider theme={cokoTheme}>
      <Wrapper>
        <TopMenu>
          <MainMenuToolBar />
        </TopMenu>
        <Main>
          <EditorArea>
            <WaxSurfaceScroll>
              <EditorContainer>{editor}</EditorContainer>
            </WaxSurfaceScroll>
          </EditorArea>
        </Main>
      </Wrapper>
    </ThemeProvider>
  );
};`

export default MyLayout;
```

## Providing config 

```javascript

import { DefaultSchema } from 'wax-prosemirror-core';

import {BaseService, BaseToolGroupService, InlineAnnotationsService, AnnotationToolGroupService,
        FullScreenService , FullScreenToolGroupService}

export default {
  MenuService: [
    {
      templateArea: 'mainMenuToolBar',
      toolGroups: [
        'Base',
        {
          name: 'Annotations',
          more: [
            'Superscript',
            'Subscript',
            'SmallCaps',
            'Underline',
            'StrikeThrough',
          ],
        },
        'FullScreen',
      ],
    },
  ],

  SchemaService: DefaultSchema,

  services: [
    new InlineAnnotationsService(),
    new AnnotationToolGroupService(),
    new BaseService(),
    new BaseToolGroupService(),
    new FullScreenService(),
    new FullScreenToolGroupService(),
  ],
};


```



## Putting them all together
 
 ```javascript
  import { Wax } from 'wax-prosemirror-core';
  import  MyLayout  from './layout';
  import  config  from './config';



   <Wax
      config={config}
      autoFocus
      placeholder="Type something..."
      value=''
      layout={MyLayout}
      onChange={source => console.log(source)}
    />


 ```
