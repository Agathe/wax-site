---
title: "Menus"
draft: false
weight: 5
Intro: "This text will appear in the search result"
class: documentation
---

## Creating a Tool 


Wax has a core Service called MenuService that lets you render tool groups for your editor.

Inside your service's register method you can bind a new tool ascosciated with that Service.
``` javascript

import { Service } from "wax-prosemirror-services";
import DummyTool from './DummyTool';

class DummyService extends Service {
  name = 'DummyService'
  boot() {}

  register() {
    this.container.bind('DummyTool').to(DummyTool);
  }
}
```
All tools must extend the `Tools` class.
You can specify `Title, Label, Name, Icon` properties for you tool.
Also `Run, Active, Select, Enable` methods can be specified for the tool.  
`renderTool` method is optional. If not specified wax will render a button, otherwise will render
the React component specified here.

```javascript
import React from 'react';
import { isEmpty } from 'lodash';
import { injectable } from 'inversify';
import DumyComponent  from './DumyComponent';
import { Tools } from 'wax-prosemirror-services';

export default
@injectable()
class DummyTool extends Tools {
  title = 'Dummy';
  label = 'Dummy';
  name = 'Dummy';
  icon = 'DummyIcon';


  get run() {
    return (state, dispatch) => {
     // execute command
    };
  }

  get active() {
    return state => {

    };
  }

  select = (state, activeViewId) => {
    
  };

  get enable() {
    return state => {
    };
  }

  renderTool(view) {
    if (isEmpty(view)) return null;
    // eslint-disable-next-line no-underscore-dangle
    return this._isDisplayed ? (
      <DumyComponent key="Dummy" item={this.toJSON()} view={view} />
    ) : null;
  }
}

```

## Create a Toolgroup

Wax will only render Toolgroups as you have already seen in the config MenuService configuration. Toolgroups may consist of one or more tools.
You can create new services for the tool groups. For example Wax Base Service consists of undo, redo and save tools.

```javascript

import {Service} from 'wax-prosemirror-services';
import Base from './Base';

class BaseToolGroupService extends Service {
  register() {
    this.container.bind('Base').to(Base);
  }
}

export default BaseToolGroupService;

```

```javascript
import { injectable, inject } from 'inversify';
import {ToolGroup} from 'wax-prosemirror-services';

@injectable()
class Base extends ToolGroup {
  tools = [];
  constructor(
    @inject('Undo') undo,
    @inject('Redo') redo,
    @inject('Save') save,
  ) {
    super();
    this.tools = [undo, redo, save];
  }
}

export default Base;
```

A `renderTools` method is also available that will enable you to render your custom Component. If not specified Wax will render a `div` and place
each tool inside.

