---
title: "Wax Context"
draft: false
weight: 4
Intro: "This text will appear in the search result"
class: documentation
---

## Using Wax Context inside your components
You can use WaxContext to communicate your React Component to the mounted prosemirror instace. WaxContext holds the following properties that
you can access.

```javascript
import React, { useContext } from 'react';
import styled from 'styled-components';
import { WaxContext } from 'wax-prosemirror-core'


const { app, activeView, view, activeViewId } = useContext(WaxContext);
```
 app holds all of the editor's config. For Example if we want to access the config passed to a service from within the component
 we can access it as follows.
 ```javascript
  const enableService = app.config.get('config.EnableTrackChangeService');
```

`view` is an object that holds all the different prosemirror views registered. The mounted prosemirror instance passed in your Layout will always 
be called and accessed as `main`.

`activeView` is prosemirror view currently the cursor is in.
`activeViewId` is the ID of that view.

## Setting a new prosemirror view inside context

For example in Notes implementation for Editoria, we have multiple prosemirror views that will be rendered. We can add that view to context
so we can access it in any of the components. 

```javascript
  const context = useContext(WaxContext);
  const noteId = node.attrs.id;


 // Set Each note into Wax's Context
    context.updateView(
      {
        [noteId]: noteView,
      },
      noteId,
    );
```

Or in every `dispatchTransaction` transaction made by the note editor setting it as the activeView.

`  context.updateView({}, noteId);`
