---
title: "About"
date: 2019-09-04T15:28:16+02:00
draft: false
class: about
intro: "For more information contact [adam@coko.foundation](mailto:adam@coko.foundation)"
---

Wax is a framework for building web-based word processors.

We have built this software because publishing, on the whole, lacks high quality open source tools. One of the critical missing pieces has been a web-based ‘editor’ that publishers of all kinds can use. Publishers are professsional text workers and they need higher quality, more accurate, text editing tools than most web applications require. Publishers also require functionality that other types of web production paradigms generally do not. Track changes, citations and notes are just three examples.

For this reason, we could not rely on any existing open source tools to do the job because there were none that we found which satisfied the requirements of publishers. So we decided to build a web-based word processor, co-designed with publishers. The result is Wax.

Wax derives its name from the first read-writable surface - wax tablets. We adopted this name when planning the tool at a meeting in Athens, Greece after seeing such tablets in the Acropolis museum. Their history was fresh in our minds.

Wax is an extensible framework. It is highly configurable and extensible. Wax can satisfy a very wide spectrum of use cases.

If you are a strategic decision maker and you are thinking ‘where is this Wax thing’ then you may want to have a quick look at the [demo](https://demo.waxjs.net/) provided. If you are a developer you may wish to look at the docs provided. In either case, if you want more information, if you would like to collaborate, or if you want to commission us to extend Wax then drop us a line at [adam@coko.foundation](mailto:adam@coko.foundation). Join the chat here [https://mattermost.coko.foundation/](https://mattermost.coko.foundation/).

Wax is 100% open source. It is yours, forever. We hope you like it. 
